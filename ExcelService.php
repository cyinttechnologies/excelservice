<?php
/*ExcelService.php
Service for creating Excel documents
Copyright (C) 2016,2017 Daniel Fredriksen
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

namespace CYINT\ComponentsPHP\Services;

class ExcelService
{
    public function convertEntitiesToExcel($entities, $Factory, $entity_name)
    {
        $objPHPExcel = new \PHPExcel();

        $objPHPExcel->getProperties()->setCreator($entity_name)
            ->setLastModifiedBy('')
            ->setTitle($entity_name . ' List')
            ->setSubject()
            ->setDescription()
            ->setCategory();

        $active_sheet = $objPHPExcel->setActiveSheetIndex(0);
        $fields = $Factory->getFieldKeys();

        $this->setHeaders($fields, $active_sheet);
        $this->setBodyOfEntities($entities, $active_sheet, $fields, $Factory);
        $filename = $this->writeToFile($objPHPExcel);
        return $filename;
    }

    public function convertToExcel($data, $sheet_name)
    {
        $objPHPExcel = new \PHPExcel();
        $fields = null;
        $objPHPExcel->getProperties()->setCreator($sheet_name)
            ->setLastModifiedBy('')
            ->setTitle($sheet_name . ' List')
            ->setSubject()
            ->setDescription()
            ->setCategory();

        $active_sheet = $objPHPExcel->setActiveSheetIndex(0);
        if(count($data) > 0)
            $fields = array_keys($data[0]);

        $this->setHeaders($fields, $active_sheet);
        $this->setBody($fields, $data, $active_sheet);
        $filename = $this->writeToFile($objPHPExcel);
        return $filename;
    }

    protected function setBody($fields, $data, &$active_sheet)
    {
        if(!empty($data))
        {
            $row = 2;
            foreach($data as $item)
            {
                $index = 0;
                foreach($fields as $field)
                {
                    $value = null;
                    $column = \PHPExcel_Cell::stringFromColumnIndex($index);
                    if(isset($item[$field])) {
                        $value = $item[$field];
                    }
                    $active_sheet->setCellValue($column . $row, $value);
                    $index++;
                }

                $row++;
            }
        }

    }

    protected function setBodyOfEntities($entities, &$active_sheet, $fields, $Factory)
    {
        if(!empty($entities))
        {
            $row = 2;
            foreach($entities as $Entity)
            {
                $index = 0;
                foreach($fields as $field)
                {
                    $column = \PHPExcel_Cell::stringFromColumnIndex($index);
                    $value = $Factory->convertFieldToString($field, $Entity);
                    $active_sheet->setCellValue($column . $row, $value);
                    $index++;
                }

                $row++;
            }
        }

    }

    protected function setHeaders($fields, &$active_sheet)
    {
        if(!empty($fields))
        {
            $index = 0;
            foreach($fields as $field)
            {
                $column = \PHPExcel_Cell::stringFromColumnIndex($index);
                $active_sheet->setCellValue($column .'1', $field);
                $index++;
            }
        }
    }

    protected function writeToFile($objPHPExcel)
    {
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $ext = '.xlsx';
        $filename = '/tmp/'. time(). $ext;
        $objWriter->save($filename);
        return $filename;
    }
}

?>
